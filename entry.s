        .section .text
        .global _start
        .type _start, @function
_start:
        jmp program_main
        .size _start, . - _start
