static long syscall(long num, long rdi, long rsi, long rdx, long r10, long r8, long r9) {
    long ret;
    register long r10_ asm("r10") = r10;
    register long r8_ asm("r8") = r8;
    register long r9_ asm("r9") = r9;
    asm volatile("syscall\n"
                 : "=a"(ret)
                 : "a"(num), "D"(rdi), "S"(rsi), "d"(rdx), "r"(r10_), "r"(r8_), "r"(r9_)
                 : "memory", "cc");
    return ret;
}

static long write(int fd, const void *buf, long count) {
    return syscall(1, fd, (long)buf, count, 0, 0, 0);
}

static void exit(int code) {
    syscall(60, code, 0, 0, 0, 0, 0);
}

static int strlen(const char *s) {
    int i;
    for (i = 0; s[i] != '\0'; i++);
    return i;
}

static const char *message = "Hello, world!\n";

extern "C" void program_main(void) {
    write(1, message, strlen(message));
    exit(0);
}
