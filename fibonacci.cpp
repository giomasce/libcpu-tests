static long syscall(long num, long rdi, long rsi, long rdx, long r10, long r8, long r9) {
    long ret;
    register long r10_ asm("r10") = r10;
    register long r8_ asm("r8") = r8;
    register long r9_ asm("r9") = r9;
    asm volatile("syscall\n"
                 : "=a"(ret)
                 : "a"(num), "D"(rdi), "S"(rsi), "d"(rdx), "r"(r10_), "r"(r8_), "r"(r9_)
                 : "memory", "cc");
    return ret;
}

static long write(int fd, const void *buf, long count) {
    return syscall(1, fd, (long)buf, count, 0, 0, 0);
}

static void exit(int code) {
    syscall(60, code, 0, 0, 0, 0, 0);
}

static void print_int(int fd, int num) {
    if (num < 0) {
        write(fd, "-", 1);
        print_int(fd, -num);
    } else if (num == 0) {
        write(fd, "0", 1);
    } else {
        int last_digit = num % 10;
        int other_digits = num / 10;
        if (other_digits != 0) {
            print_int(fd, other_digits);
        }
        char c = '0' + last_digit;
        write(fd, &c, 1);
    }
}

static int fibonacci(int x) {
    if (x <= 1) {
        return x;
    }
    return fibonacci(x - 1) + fibonacci(x - 2);
}

extern "C" void program_main(void) {
    int fib10 = fibonacci(10);
    print_int(1, fib10);
    write(1, "\n", 1);
    exit(0);
}
