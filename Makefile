all: write.bin write.txt sort.bin sort.txt fibonacci.bin fibonacci.txt loader

loader: loader.c
	gcc -o loader loader.c -O3 -Wall -Wextra -g

entry.o: entry.s
	as -c -o entry.o entry.s

%.o: %.cpp
	g++ -c -o $@ $< -std=c++20 -ffreestanding -O3 -Wall -Wextra -pie -nostdlib -nostdinc

%.elf: %.o entry.o linker.ld
	g++ -o $@ -pie -std=c++20 -T linker.ld -Wl,-z,noexecstack entry.o $< -nostdlib -nostdinc

%.bin: %.elf
	objcopy -O binary $< $@

%.txt: %.elf
	objdump -d $< > $@

.PRECIOUS: %.elf
