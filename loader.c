#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <stdio.h>
#include <unistd.h>
#include <assert.h>
#include <errno.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    assert(argc == 3);
    errno = 0;
    unsigned long long addr = strtoull(argv[2], NULL, 0);
    assert(errno == 0);
    int fd = open(argv[1], O_RDONLY);
    assert(fd >= 0);
    off_t length = lseek(fd, 0, SEEK_END);
    assert(length >= 0);
    void (*code)() = mmap((void*)addr, length, PROT_READ | PROT_EXEC, MAP_SHARED | MAP_FIXED, fd, 0);
    assert(code != MAP_FAILED);
    printf("code loaded at %p\n", code);
    code();
    return 0;
}
